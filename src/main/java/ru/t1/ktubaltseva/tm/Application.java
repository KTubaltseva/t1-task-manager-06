package ru.t1.ktubaltseva.tm;

import ru.t1.ktubaltseva.tm.constant.ArgumentConst;
import ru.t1.ktubaltseva.tm.constant.CommandConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        processArgument(args);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArgument(final String[] args) {
        if (args == null || args.length < 1) return;
        final String argument = args[0];
        switch (argument) {
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.HELP:
                displayHelp();
                break;
            default:
                displayArgumentError();
        }
        System.exit(0);
    }


    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.VERSION:
                displayVersion();
                break;
            case CommandConst.ABOUT:
                displayAbout();
                break;
            case CommandConst.HELP:
                displayHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                displayCommandError();
        }
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Tubaltseva Ksenia");
        System.out.println("email: ktubaltseva@t1-consulting.ru");
    }

    private static void exit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s\t- Display program version.\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s\t- Display developer info\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s\t- Display list of commands.\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s\t\t- Close Application.\n", CommandConst.EXIT);
    }

    private static void displayArgumentError() {
        System.err.println("Invalid argument");
        System.exit(1);
    }

    private static void displayCommandError() {
        System.err.println("Invalid command");
        displayHelp();
    }

}